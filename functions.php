<?php
// Здесь будут вспомогательные функции
session_start();

function getAuthorizedUser() {
    return $_SESSION['user'];
}

/**
 * Функция реализует мезанизм авторизации
 * @return bool Успешно ли авторизовался
 */
function login($login, $password) {
    $user = getUser($login);
    if ($user && $user['password'] == $password) {
        $_SESSION['user'] = $user;
        return true;
    }
    return false;
}

/**
 *  Получает пользователя по его логину
 */
function getUser($login) {
    $users = getUsers();
    foreach ($users as $user) {
        if ($user['login'] == $login) {
            return $user;
        }
    }
    return null;
}

/**
 *  Получает список пользователей
 */
function getUsers() {
    $users = json_decode(file_get_contents(__DIR__ . '/Data/{login}.json'), true);
    if (empty($users)) {
        return [];
    }
    return $users;
}

function redirect($page) {
    header("Location: $page.php");
    die;
}

function logout() {
    session_destroy();
}