<?php
require_once __DIR__ . '/functions.php';

if (!empty($_GET) && isset($_GET)):
    $number = $_GET["test_number"];
    if (!file_exists(__DIR__ . '/Tests/test' . $number . '.json')):
        header($_SERVER["SERVER_PROTOCOL"] . '404 Not Found');
        exit;
    endif;
    unlink(__DIR__ . '/Tests/test' . $number . '.json');
    redirect('list');
endif;
if (!empty($_SESSION['user']['password'])):?>
    <br>
    <div><a href="admin.php"><button>Вернуться к созданию и загрузке тестов</button></a></div>
<?php endif;?>

<p>Список доступных тестов: </p>
<?php
$test_files = glob('Tests/*.json');
if (!empty($test_files)):
    foreach ($test_files as $file): ?>
        <p><?php echo basename($file)?></p>
    <?php endforeach;
endif;
?>

<form action="test.php" method="GET">
    <div>Введите номер теста <input type="text" name="test_number"></div>
    <br>
    <div><input type="submit" name="get test" value="Пройти тест"><div>
</form>

<?php
if (!empty($_SESSION['user']['password'])):?>
    <form action="" method="GET">
        <br>
        <div>Введите номер теста <input type="text" name="test_number"></div>
        <br>
        <div><input type="submit" name="delete test" value="Удалить тест"><div>
    </form>
<?php endif;?>
