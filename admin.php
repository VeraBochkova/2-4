<?php
require_once __DIR__ . '/functions.php';

if (!empty($_SESSION['user']['password'])){?>
    <div>Добро пожаловать, <?= getAuthorizedUser()['username']; ?></div>
    <br>
    <form action="index.php" method="GET">
        <div><input type="submit" name="exit" value="Выход"><div>
    </form>
    <p>Выберите .json файл с тестом для загрузки на сервер:</p>
    <form action="" method="POST" enctype="multipart/form-data">
        <div><input type="file" name="test"></div>
        <br>
        <div><input type="submit" value="Отправить"></div>
        <br>
    </form>
<?php }
else {
    header($_SERVER["SERVER_PROTOCOL"] . '403 Forbidden Error');
    die;
}

$upload_dir = 'Tests/';
if (!empty($_FILES) && array_key_exists('test', $_FILES)) {
    if (move_uploaded_file($_FILES['test']['tmp_name'], $upload_dir . $_FILES['test']['name'])) {
        header('Location: list.php');
        exit;
    }
    else {
        echo ' Ошибка загрузки файла. ';
    }
}
?>